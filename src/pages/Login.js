import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext'
import { Redirect } from 'react-router-dom';

export default function Login(){

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isActive, setIsActive] = useState(false);

	const{user, setUser} = useContext(UserContext);

	useEffect(() => {
		if(email !== '' && password !== ''){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [email, password])

	function authenticateUser(e){
		e.preventDefault() //prevent default form behavior, so that the form does not submit

		// localStorage.setItem allows us to save a key/value pair to localStorage
		// Syntax: (key, value)

		/*
			ACTIVITY:
			Create a fetch request inside this function to allow users to log in. Log in the console the response.
		*/

		/*
			Test email && password
			email: test@mail.com
			password: test123
		*/

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				localStorage.setItem('email', email)

				// setEmail("")
				// setPassword("")
				setUser({
					email: email
				})

				alert("Thank you for logging in!");
				console.log(data)
			}else{
				alert("Email or Password does not match!");
			}
		})


		// alert('Thank you for logging in');

	}

	return(
		<>
			{(user.email === null) ?
				<Form onSubmit={e => authenticateUser(e)}>
					<Form.Group controlId="userEmail">
						<Form.Label>Email Address</Form.Label>
						<Form.Control
							type="email"
							placeholder="Enter email"
							value={email}
							onChange={e => setEmail(e.target.value)}
							required
						/>
						<Form.Text className="text-muted">
							We'll never share your email with anyone else.
						</Form.Text>
					</Form.Group>

					<Form.Group controlId="password">
						<Form.Label>Password</Form.Label>
						<Form.Control
							type="password"
							placeholder="Enter password"
							value={password}
							onChange={e => setPassword(e.target.value)}
							required
						/>
					</Form.Group>

					{isActive ?
						<Button className="mt-3" variant="primary" type="submit" id="submitBtn">
						Submit
						</Button>
						:
						<Button className="mt-3" variant="primary" id="submitBtn" disabled>
						Submit
						</Button>				
					}
				</Form>
				:
				<Redirect to="/"/>

			}

		</>
	)
}